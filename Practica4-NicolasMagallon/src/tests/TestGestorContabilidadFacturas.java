package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestGestorContabilidadFacturas {
	private static final int CANTIDAD_FACTURAS = 3;
	private GestorContabilidad gestor;
	
	@BeforeEach
	void setUp() {
		gestor = new GestorContabilidad();
		Cliente cliente1 = new Cliente("Alberto", "11112", "1994-04-21");
		Cliente cliente2 = new Cliente("Fernando", "11113", "1986-12-07");
		Cliente cliente3 = new Cliente("Jorge", "11114", "1990-03-14");
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
		gestor.altaCliente(cliente3);
		Factura factura1 = new Factura("1F", "2007-12-14", "Patatas", 3, 4, cliente1);
		Factura factura2 = new Factura("2F", "2004-03-08", "Zanahorias", 1, 2, cliente1);
		Factura factura3 = new Factura("3F", "2012-07-23", "Zanahorias", 5, 2, cliente3);
		gestor.crearFactura(factura1);
		gestor.crearFactura(factura2);
		gestor.crearFactura(factura3);
	}
	
	// COMPROBAR SI SE PUEDEN CREAR FACTURAS
	@Test
	void testCrearFactura() {
		gestor.crearFactura(new Factura("4F", "1994-04-21", "Almendras", 3, 4, gestor.buscarCliente("11112")));
		assertEquals(CANTIDAD_FACTURAS + 1, gestor.getListaFacturas().size());
	}
	
	// COMPROBAR SI DOS FACTURAS CON EL MISMO CODIGO PUEDEN SER A�ADIDAS
	@Test
	void crearFacturaRepetida() {
		gestor.crearFactura(new Factura("1F", "2004-12-14", "Lechugas", 1, 2, gestor.buscarCliente("11112")));
		assertEquals(CANTIDAD_FACTURAS, gestor.getListaFacturas().size());
	}
	
	// BUSCAR UNA FACTURA EN UNA LISTA VAC�A
	@Test
	void buscarFacturaListaVacia() {
		gestor = new GestorContabilidad();
		Factura factura1 = gestor.buscarFactura("1F");
		assertNull(factura1);
	}
	
	// BUSCAR UNA FACTURA QUE EXISTE EN LA LISTA DE FACTURAS
	@Test
	void buscarFacturaExistente() {
		Factura factura1 = gestor.buscarFactura("1F");
		assertEquals("1F", factura1.getCodigoFactura());
	}
		
	// BUSCAR UNA FACTURA QUE NO EXISTE EN LA LISTA DE FACTURAS
	@Test
	void buscarFacturaInexistente() {
		Factura factura1 = gestor.buscarFactura("4F");
		assertNull(factura1);
	}
	
	// BUSCAR LA FACTURA M�S CARA
	@Test
	void testFacturaMasCara() {
		Factura facturaCara = gestor.facturaMasCara();
		assertEquals(3, facturaCara.getPrecioUnidad());
	}
	
	// BUSCAR LA FACTURA M�S CARA EN UNA LISTA VAC�A
	@Test
	void facturaMasCaraListaVac�a() {
		gestor = new GestorContabilidad();
		Factura facturaCara = gestor.facturaMasCara();
		assertNull(facturaCara);
	}
	
	// CALCULAR LA FACTURACION DE UN A�O CONCRETO CON UN PRODUCTO
	@Test
	void testCalcularFacturacionAnual() {
		double facturacionAnual = gestor.calcularFacturacionAnual(2004);
		assertEquals(2, facturacionAnual);
	}
	
	// CALCULAR LA FACTURACI�N DE UN A�O CONCRETO CON VARIOS PRODUCTOS
	@Test
	void CalcularFacturacionAnualVariosProductos() {
		gestor.crearFactura(new Factura("4F", "2004-03-27", "Platanos", 4, 4, gestor.buscarCliente("11112")));
		double facturacionAnual = gestor.calcularFacturacionAnual(2004);
		assertEquals(18, facturacionAnual);
	}
	
	// DEVOLVER LA FACTURACI�N DE UN A�O DEL QUE NO HAY REGISTROS
	@Test
	void CalcularFacturacionAnualAnnoNoRegistrado() {
		double facturacionAnual = gestor.calcularFacturacionAnual(2008);
		assertEquals(0, facturacionAnual);
	}
	
	// ASIGNAR UN CLIENTE A UNA FACTURA
	@Test
	void testAsignarClienteAFactura() {
		gestor.asignarClienteAFactura("11113", "1F");
		Factura factura = gestor.buscarFactura("1F");
		Cliente cliente = gestor.buscarCliente("11113");
		assertEquals(cliente, factura.getCliente());
	}
	
	// ASIGNAR UN CLIENTE INEXISTENTE A UNA FACTURA
	@Test
	void asignarClienteInexistenteAFactura() {
		gestor.crearFactura(new Factura("4F", "2004-03-27", "Platanos", 4, 4, null));
		gestor.asignarClienteAFactura("22222", "4F");
		Factura factura = gestor.buscarFactura("4F");
		assertNull(factura.getCliente());
	}
	
	// ASIGNAR UN CLIENTE A UNA FACTURA INEXISTENTE
	@Test
	void asignarClienteAFacturaInexistente() {
		gestor.asignarClienteAFactura("11112", "4F");
		Factura facturaInexistente = gestor.buscarFactura("4F");
		assertNull(facturaInexistente);
	}
	
	// ENCONTRAR LA CANTIDAD DE FACTURAS DE UN CLIENTE
	@Test
	void testCantidadFacturasPorCliente() {
		int cantidadFacturas = gestor.cantidadFacturasPorCliente("11112");
		assertEquals(2, cantidadFacturas);
	}
	
	// CANTIDAD DE FACTURAS DE UN CLIENTE SIN FACTURAS
	@Test
	void CantidadFacturasPorClienteSinFacturas() {
		int cantidadFacturas = gestor.cantidadFacturasPorCliente("11113");
		assertEquals(0, cantidadFacturas);
	}
	
	// CANTIDAD DE FACTURAS DE UN CLIENTE INEXISTENTE
	@Test
	void cantidadFacturasPorClienteInexistente() {
		int cantidadFacturas = gestor.cantidadFacturasPorCliente("11115");
		assertEquals(0, cantidadFacturas);
	}
	
	// COMPROBAR QUE ELIMINA FACTURAS CORRECTAMENTE
	@Test
	void testEliminarFactura() {
		gestor.eliminarFactura("1F");
		assertEquals(CANTIDAD_FACTURAS - 1, gestor.getListaFacturas().size());
	}
	
	// COMPROBAR QUE NO DA ERROR AL ELIMINAR UNA FACTURA QUE NO EXISTE
	@Test
	void eliminarFacturaInexistente() {
		gestor.eliminarFactura("4F");
		assertEquals(CANTIDAD_FACTURAS, gestor.getListaFacturas().size());
	}
}
