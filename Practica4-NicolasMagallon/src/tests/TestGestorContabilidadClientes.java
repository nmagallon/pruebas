package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestGestorContabilidadClientes {
	private static final int CANTIDAD_CLIENTES = 3;
	private GestorContabilidad gestor;
	
	@BeforeEach
	void setUp() {
		gestor = new GestorContabilidad();
		Cliente cliente1 = new Cliente("Alberto", "11112", "1994-04-21");
		Cliente cliente2 = new Cliente("Fernando", "11113", "1986-12-07");
		Cliente cliente3 = new Cliente("Jorge", "11114", "1990-03-14");
		gestor.altaCliente(cliente1);
		gestor.altaCliente(cliente2);
		gestor.altaCliente(cliente3);
	}
	
	// COMPROBAR SI DOS CLIENTES CON EL MISMO DNI PUEDEN SER A�ADIDOS
	@Test
	void altaClienteRepetido() {
		Cliente cliente1 = new Cliente("Jose", "11112", "1995-02-18");
		gestor.altaCliente(cliente1);
		assertEquals(CANTIDAD_CLIENTES, gestor.getListaClientes().size());
	}
	
	// COMPROBAR SI INTRODUCE CLIENTES CON DIFERENTE DNI PERO MISMOS DATOS
	@Test
	void altaClientesCasiIguales() {
		Cliente cliente1 = new Cliente("Francisco", "11115", "1994-04-21");
		gestor.altaCliente(cliente1);
		assertEquals(CANTIDAD_CLIENTES + 1, gestor.getListaClientes().size());
	}
	
	// BUSCAR UN CLIENTE EN UNA LISTA VAC�A
		@Test
		void buscarClienteListaVacia() {
			gestor = new GestorContabilidad();
			Cliente cliente1 = gestor.buscarCliente("11112");
			assertNull(cliente1);
		}
	
	// BUSCAR UN CLIENTE QUE EXISTE EN LA LISTA DE CLIENTES
	@Test
	void buscarClienteExistente() {
		Cliente cliente1 = gestor.buscarCliente("11112");
		assertEquals("11112", cliente1.getDni());
	}
	
	// BUSCAR UN CLIENTE QUE NO EXISTE EN LA LISTA DE CLIENTES
	@Test
	void buscarClienteInexistente() {
		Cliente cliente1 = gestor.buscarCliente("11115");
		assertNull(cliente1);
	}
	
	// COMPROBAR QUE ELIMINA CLIENTES CORRECTAMENTE
	@Test
	void eliminarClienteExistente() {
		gestor.eliminarCliente("11112");
		assertEquals(CANTIDAD_CLIENTES - 1, gestor.getListaClientes().size());
	}
	
	// COMPROBAR QUE NO DA ERROR AL ELIMINAR UN CLIENTE QUE NO EXISTE
	@Test
	void eliminarClienteInexistente() {
		gestor.eliminarCliente("222222");
		assertEquals(CANTIDAD_CLIENTES, gestor.getListaClientes().size());
	}
	
	// COMPROBAR QUE ENCUENTRA EL CLIENTE M�S ANTIGUO
	@Test
	void testClienteMasAntiguo() {
		Cliente cliente1 = new Cliente("Federico", "11115", "1950-07-07");
		gestor.altaCliente(cliente1);
		assertEquals(cliente1, gestor.clienteMasAntiguo());
	}
	
	// BUSCAR EL CLIENTE M�S ANTIGUO ENTRE DOS CLIENTES DEL MISMO A�O
	@Test
	void clienteMasAntiguoDelMismoAnyo() {
		Cliente cliente1 = new Cliente("Francisco", "11115", "1986-11-05");
		gestor.altaCliente(cliente1);
		Cliente clienteAntiguo = gestor.clienteMasAntiguo();
		assertEquals(cliente1, clienteAntiguo);
	}
	
	// BUSCAR EL CLIENTE M�S ANTIGUO EN UNA LISTA VAC�A
	@Test
	void clienteMasAntiguoListaVacia() {
		gestor = new GestorContabilidad();
		Cliente clienteAntiguo = gestor.clienteMasAntiguo();
		assertNull(clienteAntiguo);
	}
}