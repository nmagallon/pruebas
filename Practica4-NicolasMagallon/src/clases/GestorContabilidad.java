package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura>listaFacturas;
	private ArrayList<Cliente>listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	public Cliente buscarCliente(String dni) {
		for(Cliente cliente : listaClientes) {
			if(cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigoFactura) {
		for(Factura factura : listaFacturas) {
			if(factura.getCodigoFactura().equals(codigoFactura)) {
				return factura;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente cliente) {
		if(!listaClientes.contains(cliente)) {
			listaClientes.add(cliente);
		}
	}
	
	public void crearFactura(Factura factura) {
		if(!listaFacturas.contains(factura)) {
			listaFacturas.add(factura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		if(listaClientes.size() > 0) {
			Cliente clienteAntiguo = listaClientes.get(0);
			if(clienteAntiguo != null) {
				for(Cliente cliente : listaClientes) {
					if(clienteAntiguo.getFechaAlta().isAfter(cliente.getFechaAlta())) {
						clienteAntiguo = cliente;
					}
				}
				return clienteAntiguo;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public Factura facturaMasCara() {
		if(listaFacturas.size() > 0) {
			Factura facturaCara = listaFacturas.get(0);
			if(facturaCara != null) {
				for(Factura factura : listaFacturas) {
					if(facturaCara.calcularPrecioTotal() < factura.calcularPrecioTotal()) {
						facturaCara = factura;
					}
				}
				return facturaCara;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public double calcularFacturacionAnual(int anno) {
		double facturacionAnual = 0;
		for(Factura factura : listaFacturas) {
			if(factura.getFecha().getYear() == anno) {
				facturacionAnual += factura.calcularPrecioTotal();
			}
		}
		return facturacionAnual;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Factura factura = buscarFactura(codigoFactura);
		Cliente cliente = buscarCliente(dni);
		if(factura != null && cliente != null) {
			factura.setCliente(cliente);
		}
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		Cliente cliente = buscarCliente(dni);
		int numeroFacturas = 0;
		if(cliente != null) {
			for(Factura factura : listaFacturas) {
				if(factura.getCliente().equals(cliente)) {
					numeroFacturas++;
				}
			}
			return numeroFacturas;
		} else {
			return 0;
		}
	}
	
	public void eliminarFactura(String codigoFactura) {
		listaFacturas.remove(buscarFactura(codigoFactura));
	}
	
	public void eliminarCliente(String dni) {
		listaClientes.remove(buscarCliente(dni));
	}
}