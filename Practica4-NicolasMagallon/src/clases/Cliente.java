package clases;

import java.time.LocalDate;

public class Cliente {
	private String nombre;
	private String dni;
	private LocalDate fechaAlta;
	
	public Cliente(String nombre, String dni, String fechaAlta) {
		this.nombre = nombre;
		this.dni = dni;
		this.fechaAlta = LocalDate.parse(fechaAlta);
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public boolean equals(Object obj) {
		if(this.dni.equals(((Cliente)obj).getDni())) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "Nombre: " + nombre + " | " + "DNI: " + dni + " | " + "Fecha alta: " + fechaAlta;
	}
}
